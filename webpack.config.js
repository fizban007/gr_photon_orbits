module.exports = {
    mode: "production",
    entry: ["./src/main.js"],
    output: {
        path: __dirname + "/dist",
        filename: "bundle.js"
    },
    optimization: {
        minimize: false,
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: ["style-loader", "css-loader"]
            },

            {
                test: /\.(glsl|vs|fs|vert|frag)$/,
                exclude: /node_modules/,
                use: ["raw-loader", "glslify-loader"]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            }
        ]
    },

    devServer: {
        contentBase: './dist',
        watchContentBase: true,
        historyApiFallback: true,
        host: "0.0.0.0",
        port: 8080,
        stats: "errors-only",
        noInfo: true,
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000,
            ignored: /node_modules/
        },
        // mimeTypes: { 'text/css': ['css'] }
    },

    // plugins: []
};
