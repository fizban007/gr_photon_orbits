This is a simple javascript-based visualizer of photon trajectories in GR

How to use
==========

Clone this repo, then run the following in the cloned directory:

``` sh
$ npm install
$ npm run build
$ npm run serve
```

This will start a webserver on your machine and you can see the content of the app at `localhost:8080`.