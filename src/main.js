require("aframe");
require("aframe-orbit-controls");
require("./components/dat-gui");
require("./components/axes-helper");

// const THREE = require("three");
const MeshLine = require("three.meshline").MeshLine;
const MeshLineMaterial = require("three.meshline").MeshLineMaterial;
// const MeshLineRaycast = require("three.meshline").MeshLineRaycast;

const CAM_DISTANCE = 300;

AFRAME.registerComponent("gui", {
    schema: {},

    init: function() {
        var sceneEl = this.el;
        var gui = document.querySelector("a-scene").systems["dat-gui"].gui;
        var traj = document.querySelector("a-entity[trajectory]").components
            .trajectory;

        // Speed of the trajectories
        gui.add(traj.data, "speed").min(0.0).max(1.0);
        // Color of the trajectories
        // TODO: Add a way to let different lines have different colors
        gui.addColor(traj.data, "color");
        var params = {
            bh_spin: 0.9375,
            bg: "#d7d7d7",
            loadFile: function() {
                document.getElementById("jsonUpload").click();
            }
        };
        // Color of the background
        gui.addColor(params, "bg")
            .name("background")
            .onChange(function() {
                sceneEl.object3D.background = new THREE.Color(params.bg);
            });

        // Allow changing the spin of the black hole, therefore changing the
        // size of the horizon
        var horizon = document.getElementById("horizon");
        horizon.attributes.radius.value =
            1.0 + Math.sqrt(1.0 - params.bh_spin * params.bh_spin);
        gui.add(params, "bh_spin")
            .name("a")
            .step(0.0001)
            .min(0.0)
            .max(1.0)
            .onChange(function() {
                horizon.attributes.radius.value =
                    1.0 + Math.sqrt(1.0 - params.bh_spin * params.bh_spin);
            });

        // File uploader button
        gui.add(params, "loadFile").name("Load JSON file");
        var upload = document.getElementById("jsonUpload");
        upload.addEventListener("change", function() {
            if (upload.files && upload.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    traj.json_response = JSON.parse(e.target.result);
                    traj.load_json_trajectories();
                };
                reader.readAsText(upload.files[0]);
            }
        });

        // Screenshot button
        params.screenshot = function() {
            document
                .querySelector("a-scene")
                .components.screenshot.capture("perspective");
        };
        gui.add(params, "screenshot");

        // Play toggles whether the shooting animation is playing
        params.play = function() {
            traj.data.playing = !traj.data.playing;
            if (traj.data.playing)
                play_button.name("pause");
            else
                play_button.name("play");
        };
        play_button = gui.add(params, "play").name("pause");

        // Dynamic toggles wether the photon trajectories are static or playing
        params.dynamic = function() {
            if (!traj.data.dynamic) {
                dyn_button.name("static");
            } else {
                dyn_button.name("dynamic");
            }
            traj.set_dynamic(!traj.data.dynamic);
        };
        dyn_button = gui.add(params, "dynamic").name("static");
    }
});

AFRAME.registerComponent("trajectory", {
    schema: {
        dir: { type: "string", default: "./data/" },
        filename: { type: "string", default: "photon_data.json" },
        color: { type: "color", default: "#199d19" },
        dashArray: { type: "number", default: 2 },
        dashRatio: { type: "number", default: 0.75 },
        dynamic: { type: "boolean", default: true },
        playing: { type: "boolean", default: true },
        speed: { type: "number", default: 0.5 },
    },

    make_trajectory: function(data) {
        var line = new MeshLine();
        const points = [];
        var r_max = 40.0;
        // Go over the coordinates. Note that a-frame uses a different coordinate
        // orientation than we are used to, so we need to swap Y and Z (and invert Y
        // to preserve chirality)
        for (let i = 0; i + 2 < data.length; i += 3) {
            // Some of this sanitation may not be necessary
            if (isNaN(data[i]) || isNaN(data[i + 1]) || isNaN(data[i + 2]))
                continue;
            if (
                Math.abs(data[i]) > r_max ||
                    Math.abs(data[i + 1]) > r_max ||
                    Math.abs(data[i + 2]) > r_max
            )
                continue;
            points.push(data[i], -data[i + 2], data[i + 1]);
        }
        line.setPoints(points, function(p) {
            return 0.02;
        });
        const material = new MeshLineMaterial({
            color: this.data.color,
            dashArray: this.data.dashArray,
            dashOffset: 0,
            dashRatio: this.data.dashRatio,
            transparent: true,
            opacity: 1,
            depthWrite: false
        });
        const mesh = new THREE.Mesh(line, material);
        return mesh;
    },

    load_json_trajectories: function() {
        var mesh = new THREE.Group();
        // Try to get a total number of lines in the response
        var num = 0;
        for (const [key, data] of Object.entries(this.json_response)) {
            if (!Array.isArray(data)) continue;
            if (!Array.isArray(data[0]))
                num += 1;
            else
                num += data.length;
            // This only goes to the first level, so if there is a triple array then
            // we don't get the correct number...
        }

        // Actually create the lines
        for (const [key, data] of Object.entries(this.json_response)) {
            if (!Array.isArray(data)) continue;
            if (!Array.isArray(data[0])) {
                var line = this.make_trajectory(data);
                mesh.add(line);
            } else {
                for (let i = 0; i < data.length; i++) {
                    var line = this.make_trajectory(data[i]);
                    mesh.add(line);
                }
            }
        }
        this.el.setObject3D("mesh", mesh);
    },

    /**
     * Initial creation and setting of the mesh.
     */
    init: function() {
        var obj = this;
        var json_loader = new THREE.FileLoader();

        json_loader.load(this.data.dir + this.data.filename, function(content) {
            obj.json_response = JSON.parse(content);
            obj.load_json_trajectories();
        });
    },

    tick: function() {
        var el = this.el;
        var mesh = el.getObject3D("mesh");
        if (mesh) {
            for (line of mesh.children) {
                line.material.color.set(this.data.color);

                if (this.data.dynamic && this.data.playing) {
                    // Decrement the dashOffset value to animate the path with the dash.
                    line.material.uniforms.dashOffset.value -= this.data.speed * 0.02;

                    // Check if the dash is out to restart animate it.
                    if (line.material.uniforms.dashOffset.value < -2)
                        line.material.uniforms.dashOffset.value = 0;
                }
            }
        }
    },

    set_dynamic: function(isDynamic) {
        if (this.data.dynamic != isDynamic) {
            this.data.dynamic = isDynamic;
            if (this.data.dynamic) {
                this.data.dashArray = 2;
                this.data.dashRatio = 0.75;
            } else {
                this.data.dashArray = 1;
                this.data.dashRatio = 0;
            }
            var mesh = this.load_json_trajectories();
            this.el.setObject3D("mesh", mesh);
        }
    }
});

