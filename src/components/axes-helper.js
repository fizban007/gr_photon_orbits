const dragElement = require("../drag-element").dragElement;

const CANVAS_WIDTH = 200;
const CANVAS_HEIGHT = 200;
const CAM_DISTANCE = 300;

AFRAME.registerComponent("axes_helper", {
    schema: {},

    init: function() {
        var sceneEl = this.el.sceneEl;
        var camera = sceneEl.camera;
        console.log(camera);
        
        // dom
        container2 = document.getElementById('inset');
        // Make the DIV element draggable:
        dragElement(container2);

        // renderer
        this.renderer2 = new THREE.WebGLRenderer( { alpha: true, });
        this.renderer2.setClearColor( 0x000000, 0 );
        this.renderer2.setSize( CANVAS_WIDTH, CANVAS_HEIGHT );
        container2.appendChild( this.renderer2.domElement );

        // scene
        this.scene2 = new THREE.Scene();
        this.scene2.background = null;

        // camera
        this.camera2 = new THREE.PerspectiveCamera( 50, CANVAS_WIDTH / CANVAS_HEIGHT, 1, 1000 );
        this.camera2.up = camera.up; // important!

        // axes
        // axes2 = new THREE.AxisHelper( 100 );
        const origin = new THREE.Vector3(0, 0, 0);
        const z_hat = new THREE.Vector3(0, 1, 0);
        const y_hat = new THREE.Vector3(0, 0, -1);
        const x_hat = new THREE.Vector3(1, 0, 0);

        var axes2 = new THREE.Group();
        const x_arrow = new THREE.ArrowHelper(x_hat, origin, 100, 0xff0000, 20, 15);
        const y_arrow = new THREE.ArrowHelper(y_hat, origin, 100, 0x00ff00, 20, 15);
        const z_arrow = new THREE.ArrowHelper(z_hat, origin, 100, 0x0000ff, 20, 15);

        axes2.add(x_arrow);
        axes2.add(y_arrow);
        axes2.add(z_arrow);
        this.scene2.add( axes2 );       
    },

    tick: function() {
        var sceneEl = this.el.sceneEl;
        var camera = sceneEl.camera;
        
        this.camera2.position.copy( camera.position );
	    this.camera2.position.setLength( CAM_DISTANCE );

        this.camera2.lookAt( this.scene2.position );
    },

    tock: function() {
        this.renderer2.render(this.scene2, this.camera2);
    }
});
